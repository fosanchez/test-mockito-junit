package com.tower.crtn.test.mockito.junit.exception.model;

public enum ErrorType {

  ERROR, WARN, INVALID, FATAL

}
